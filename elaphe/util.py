# coding: utf-8
from __future__ import print_function
from os.path import abspath, dirname, join as pathjoin
from binascii import hexlify
from textwrap import TextWrapper
import re

__all__ = ['DEFAULT_PS_CODE_PATH', 'DEFAULT_DISTILL_RE',
           'to_ps', 'cap_unescape', 'dict_to_optstring', 'distill_ps_code',
           'DEFAULT_EPSF_DSC_TEMPLATE', 'DEFAULT_RENDER_COMMAND_TEMPLATE',
           'init_ps_code_template', 'BARCODE_PS_CODE_PATH', 'PS_CODE_TEMPLATE']


# default barcode.ps path and distiller regexp.
DEFAULT_PS_CODE_PATH = pathjoin(
    dirname(abspath(__file__)), 'postscriptbarcode', 'barcode.ps')
DEFAULT_DISTILL_RE = re.compile(r'% --BEGIN TEMPLATE--(.+)% --END TEMPLATE--', re.S)


def _bin(n):
    return str(n) if n<=1 else _bin(n >> 1) + str(n & 1)
try:
    bin
except NameError:
    bin = lambda n: '0b'+_bin(n)


def zf_bin(n, width):
    return bin(n)[2:].zfill(width)[0-width:]


_cap_escape_re = re.compile(r'^\^\d\d\d')


def cap_unescape(msg):

    bits = []
    while msg:
        if _cap_escape_re.search(msg):
            oct_ord, msg = msg[1:4], msg[4:]
            bits.append(chr(int(oct_ord, 10)%256))
        else:
            bits.append(msg[0])
            msg = msg[1:]
    return ''.join(bits)


def to_ps(obj, parlen=False):

    if isinstance(obj, str):
        ret = '%s' %obj
        if parlen:
            ret = '(%s)' % ret
    elif isinstance(obj, bool):
        ret = {True: 'true', False: 'false'}[obj]
    elif isinstance(obj, type(None)):
        ret = 'null'
    else:
        ret = str(obj)
    return ret


def ps_hex_str(s):
    response = TextWrapper(subsequent_indent=' ', width=180).fill('('+s+')')
    return response


def dict_to_optstring(d, none=lambda x: '<>', empty=lambda x: '<>',
                      raw_none=lambda x: '()', raw_empty=lambda x: '()',
                      raw=True):
    none_ = none
    empty_ = empty
    if raw:
        none_ = raw_none
        empty_ = raw_empty
    if d is None:
        return none_(d)
    elif d:
        ret = ' '.join(
            (key + {True: '', False:'=%s' % to_ps(value)}[value is True])
            for key, value in list(d.items()) if not value is False)
        if raw:
            return '('+ret+')'
        else:
            return ps_hex_str(ret)
    else:
        return empty_(d)


def distill_ps_code(path_to_ps_code=DEFAULT_PS_CODE_PATH,
                    distill_regexp=DEFAULT_DISTILL_RE):
    return distill_regexp.findall(open(path_to_ps_code, 'r').read())[0].replace('%', '%%')


DEFAULT_EPSF_DSC_TEMPLATE = """%%!PS-Adobe-2.0
%%%%Pages: (attend)
%%%%Creator: Elaphe powered by barcode.ps
%%%%BoundingBox: %(bbox)s
%%%%LanguageLevel: 2
%%%%EndComments
"""
DEFAULT_RENDER_COMMAND_TEMPLATE = """
gsave
0 0 moveto
%(xscale)f %(yscale)f scale
%(codestring)s %(options)s
/%(codetype)s /uk.co.terryburton.bwipp findresource exec
grestore
showpage
"""

def init_ps_code_template(epsf_dsc_template=DEFAULT_EPSF_DSC_TEMPLATE,
                          render_command_template=DEFAULT_RENDER_COMMAND_TEMPLATE,
                          ps_code_distiller=distill_ps_code):
    """Initializes postscript code template.
    """
    return '\n'.join([epsf_dsc_template, ps_code_distiller(), render_command_template])


BARCODE_PS_CODE_PATH = distill_ps_code()
PS_CODE_TEMPLATE = init_ps_code_template()
