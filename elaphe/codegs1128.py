# coding: utf-8
from __future__ import print_function
import re
from .base import Barcode, LinearCodeRenderer, DPI


CODE128_ESCAPE_RE = re.compile(r'\^\d{3}')
CODE128_CHARS =" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
class CodeGS1128(Barcode):
    codetype = 'gs1-128'
    class _Renderer(LinearCodeRenderer):
        default_options = dict(
            LinearCodeRenderer.default_options,
            height=1, includetext=False)

        def _count_chars(self, codestring):
            mode = -1
            idx = 0
            count = 0
            while idx<len(codestring):
                if codestring[idx] == '^':
                    code_i = int(codestring[idx+1:idx+4])
                    idx += 4
                else:
                    if mode==2:
                        code_i = int(codestring[idx:idx+2])
                        idx += 2
                    else:
                        code_i = CODE128_CHARS.find(codestring[idx])
                        idx += 1
                if code_i in (101, 103):
                    mode = 0
                elif code_i in (100, 104):
                    mode = 1
                elif code_i in (99, 105):
                    mode = 2
                count+=1
            return count
            
        def _code_bbox(self, codestring):
            height = self.lookup_option('height')
            return [0, 0, self._count_chars(codestring)*6+40, height*35]

        def _text_bbox(self, codestring):
            textyoffset = self.lookup_option('textyoffset')
            textsize = self.lookup_option('textsize')
            textmaxy = textyoffset + textsize
            textmaxx = 11*self._count_chars(codestring)+0.6*textsize
            return [0, textyoffset, textmaxx, textmaxy]
        
        def build_params(self, codestring):
            params = super(CodeGS1128._Renderer, self).build_params(codestring)
            cbbox = self._code_bbox(codestring)
            if self.lookup_option('includetext'):
                tbbox = self._text_bbox(codestring)
            else:
                tbbox = cbbox
            params['bbox'] = "%d %d %d %d" %self._boundingbox(cbbox, tbbox)
            return params

    renderer = _Renderer

