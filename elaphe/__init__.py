# coding: utf-8
"""elaphe -- A Python binding for Barcode Writer In Pure Postscrpt.
"""
from .base import Barcode
from .__version__ import VERSION

DEFAULT_PLUGINS = [
    'elaphe.ean', 'elaphe.upc', 'elaphe.code128', 'elaphe.code39',
    'elaphe.code93', 'elaphe.i2of5', 'elaphe.rss', 'elaphe.pharmacode',
    'elaphe.code25', 'elaphe.code11', 'elaphe.codabar', 'elaphe.onecode',
    'elaphe.postnet', 'elaphe.royalmail', 'elaphe.auspost', 'elaphe.kix',
    'elaphe.japanpost', 'elaphe.msi', 'elaphe.plessey', 'elaphe.raw',
    'elaphe.symbol', 'elaphe.pdf417', 'elaphe.datamatrix', 'elaphe.qrcode',
    'elaphe.maxicode', 'elaphe.azteccode', 'elaphe.gs1datamatrix', 'elaphe.codegs1128']

if __name__ == "__main__":
    DEFAULT_PLUGINS = [s.replace('elaphe.', '') for s in DEFAULT_PLUGINS]
    

def load_plugins():
    for PL in DEFAULT_PLUGINS:
        try:
            __import__(PL, fromlist=[PL])
        except ImportError as e:
            import sys
            sys.stdout.write(u'Warning: %s\n' %e)
    Barcode.update_codetype_registry()
load_plugins()


def barcode(codetype, codestring, options=None, **kw):

    # search for codetype registry
    renderer = Barcode.resolve_codetype(codetype)
    if renderer:
        return renderer().render(codestring, options=options, **kw)
    raise ValueError(u'No renderer for codetype %s' %codetype)

